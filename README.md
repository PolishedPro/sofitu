
1. [Initial UI/UX Mockup](design/README.md)
2. [Notes from the Focus Group](notes/README.md)

------------------------------------------------------

#UI/UX Design Tips for sofitU

    Note: I tested the app on my Samsung Galaxy S6

### Loading Screen

    Obscure

    Too much is going on

    Logo is skewed


### Create Page
   
    This should not be the first page you see

    This should be a module you go to after pressing a '+'
    
    Location did not work
    
    Bottom icons move with the keyboard pop-up
    
    Remove two jerseys on the min and max player count


### My World
   
    Icon alignment issue at top
    
    Not clear how to add friends
    
    Profile "Log Out" button is confusing
    
    Sports on profile should be labeled differently


### Universe
   
    Doesn't show who created an event
    
    Alerts Calendar is kind of confusing


### General
   
    Back button doesn't work on Android phones
    
    Simple UI/UX tweaks can really take this to the next level
    
    Overall this is a fantastic app with an amazing concept
